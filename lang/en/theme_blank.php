<?php

/**
 * Blank Theme Moodle
 *
 * @package    theme_blank
 * @author     Michael Meneses <michael@michaelmeneses.com.br>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Blank</h2>
<p><img class=img-polaroid src="blank/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>This package provides a blank theme to facilitate cloning of the Clean Theme and develop a new theme.</p>
<h3>Source control URL:</h3>
<p><a href="https://bitbucket.org/michaelmeneses/theme_blank">https://bitbucket.org/michaelmeneses/theme_blank</a></p>
</div></div>';

$string['configtitle'] = 'Blank';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['pluginname'] = 'Blank';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

