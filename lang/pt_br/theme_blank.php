<?php

/**
 * Blank Theme Moodle
 *
 * @package    theme_blank
 * @author     Michael Meneses <michael@michaelmeneses.com.br>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Blank</h2>
<p><img class=img-polaroid src="blank/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>Sobre</h3>
<p>Este pacote fornece um tema em branco para facilitar o clone do tema Clean e desenvolver um novo tema.</p>
<h3>Código fonte:</h3>
<p><a href="https://bitbucket.org/michaelmeneses/theme_blank">https://bitbucket.org/michaelmeneses/theme_blank</a></p>
</div></div>';

$string['configtitle'] = 'Blank';

$string['customcss'] = 'CSS personalizado';
$string['customcssdesc'] = 'Seja qual for a regra CSS que você adicione a esta área de texto, a mesma será refletida em todas as páginas, tornando mais fácil a personalização deste tema.';

$string['pluginname'] = 'Blank';

$string['region-side-post'] = 'Direita';
$string['region-side-pre'] = 'Esquerda';

